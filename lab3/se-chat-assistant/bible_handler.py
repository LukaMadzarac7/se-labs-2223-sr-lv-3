from query_handler_base import QueryHandlerBase
import random
import requests
import json

class BibleHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "bible" in query:
            return True
        return False

    def process(self, query):

        try:
            result = self.call_api()
            print(result)

        except: 
            self.ui.say("Oh no! There was an error trying to contact Bible api.")
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://ajith-holy-bible.p.rapidapi.com/GetBooks"



        headers = {
            'X-RapidAPI-Key': '9b85ad80c5msh6aca4fe0f73265ep1ae18cjsn4d2e907991c3',
            'X-RapidAPI-Host': 'ajith-holy-bible.p.rapidapi.com'
        }

        response = requests.request("GET", url, headers=headers )

        return json.loads(response.text)
