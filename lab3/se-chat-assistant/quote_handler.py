from query_handler_base import QueryHandlerBase
import random
import requests
import json

class QuoteHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "quote" in query:
            return True
        return False

    def process(self, query):
        try:
            result = self.call_api()
            print('Ok, here is a random quote: \b')
            print(result['content'])
            #print(result['body'][0]['punchline'])

        except: 
            self.ui.say("Oh no! There was an error trying to contact Quote api.")
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://quotes15.p.rapidapi.com/quotes/random/"


        headers = {
    'X-RapidAPI-Key': '9b85ad80c5msh6aca4fe0f73265ep1ae18cjsn4d2e907991c3',
    'X-RapidAPI-Host': 'quotes15.p.rapidapi.com'
        }

        response = requests.request("GET", url, headers=headers)

        return json.loads(response.text)
